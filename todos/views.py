from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    tasks = TodoList.objects.get(id=id)
    context = {
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect("todo_list_detail", id=TodoList.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    ListUpdate = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=ListUpdate)
        if form.is_valid():
            ListUpdate=form.save()
            return redirect("todo_list_detail", id=ListUpdate.id)
    else:
        form = TodoListForm(instance = ListUpdate)

    context = {
        "form": form
    }
        
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    ListDelete = TodoList.objects.get(id=id)
    if request.method == "POST":
        ListDelete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            TodoItem = form.save()
            return redirect("todo_list_detail", id=TodoItem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item/create.html", context)