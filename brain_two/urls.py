"""
URL configuration for brain_two project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todos.views import todo_list_list, todo_list_detail, todo_list_create, todo_list_update, todo_list_delete, todo_item_create

urlpatterns = [
    path("admin/", admin.site.urls),
    path("todos/", todo_list_list, name = "todo_list_list"),
    path("todos/<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("todos/create/", todo_list_create, name="todo_list_create"),
    path("todos/<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("todos/<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("todos/item/create/", todo_item_create, name="todo_item_create"),
]

